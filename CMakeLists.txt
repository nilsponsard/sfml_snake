cmake_minimum_required(VERSION 3.16)
project(sfml_test)

set(CMAKE_CXX_STANDARD 14)

#set(SFML_STATIC_LIBRARIES TRUE)

find_package(SFML 2 COMPONENTS graphics audio REQUIRED)

add_executable(sfml_test main.cpp src/snake/Snake.cpp src/snake/Snake.h src/snake/direction.h src/apple/Apple.cpp src/apple/Apple.h src/constants.h src/randomGenerator/randomGenerator.cpp src/randomGenerator/randomGenerator.h)
target_link_libraries(sfml_test sfml-graphics sfml-audio)