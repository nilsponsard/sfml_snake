//
// Created by sautax on 05/05/2020.
//

#ifndef SFML_TEST_APPLE_H
#define SFML_TEST_APPLE_H
#include "SFML/Graphics.hpp"
#include <random>


namespace snakeNameSpace {


    class Apple {
    private:

        std::default_random_engine randomEngine;
        std::uniform_int_distribution<int> uniformIntDistributionX;
        std::uniform_int_distribution<int> uniformIntDistributionY;

        sf::RectangleShape rect;

    public:
        Apple();
        void setRandomPos();
        sf::RectangleShape getShape();
        sf::Vector2f getPosition();
    };
}


#endif //SFML_TEST_APPLE_H
