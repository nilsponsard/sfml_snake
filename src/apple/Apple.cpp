//
// Created by sautax on 05/05/2020.
//
#include <iostream>
#include <random>
#include "Apple.h"
#include "../constants.h"


snakeNameSpace::Apple::Apple() {
    std::random_device rd;
    std::default_random_engine engine(rd());
    randomEngine = engine;
    std::uniform_int_distribution<int> uniform_distX(1, snakeNameSpace::WINDOWWIDTH / snakeNameSpace::SNAKESIZE -2);
    uniformIntDistributionX = uniform_distX;
    std::uniform_int_distribution<int> uniform_distY(1, snakeNameSpace::WINDOWHEIGHT / snakeNameSpace::SNAKESIZE -2);
    uniformIntDistributionY = uniform_distY;
    rect = sf::RectangleShape(sf::Vector2f(0, 0));
    rect.setFillColor(sf::Color(200, 0, 0));
    rect.setSize(sf::Vector2f(SNAKESIZE, SNAKESIZE));
    setRandomPos();
}

sf::RectangleShape snakeNameSpace::Apple::getShape() {
    return rect;
}

void snakeNameSpace::Apple::setRandomPos() {
    int x = uniformIntDistributionX(randomEngine) * SNAKESIZE;
    int y = uniformIntDistributionY(randomEngine) * SNAKESIZE;
    rect.setPosition(sf::Vector2f(x, y));
    std::cout<<x<<" : "<<y<<std::endl;
}

sf::Vector2f snakeNameSpace::Apple::getPosition() {
    return rect.getPosition();
}


