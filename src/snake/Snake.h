//
// Created by sautax on 05/05/2020.
//
#ifndef SFML_TEST_SNAKE_H
#define SFML_TEST_SNAKE_H
#include <vector>
#include "SFML/Graphics.hpp"

#include "direction.h"
#include "../apple/Apple.h"


namespace snakeNameSpace{


class Snake {
private:
    std::vector<sf::Vector2f> tail;
    sf::Vector2f head;
    direction heading;
    direction wantedHeading;
    void grow();
public:
    explicit Snake(sf::Vector2f posInitial);
    int update(snakeNameSpace::Apple &);
    void trySetHeading(direction dir);
    void draw(sf::RenderTarget&);

};

}

#endif //SFML_TEST_SNAKE_H
