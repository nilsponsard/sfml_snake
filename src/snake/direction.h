//
// Created by sautax on 05/05/2020.
//

#ifndef SFML_TEST_DIRECTION_H
#define SFML_TEST_DIRECTION_H

enum direction{
    NORTH,
    EAST,
    SOUTH,
    WEST
};


#endif //SFML_TEST_DIRECTION_H
