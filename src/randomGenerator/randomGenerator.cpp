//
// Created by sautax on 05/05/2020.
//
#include <random>
#include "randomGenerator.h"


int getRandomInt(const int &min, const int &max) {

    std::random_device r;

// Choose a random mean between 1 and 6
    std::default_random_engine e1(r());
    std::uniform_int_distribution<int> uniform_dist(min, max);
    return uniform_dist(e1);
}
