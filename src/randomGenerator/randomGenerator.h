//
// Created by sautax on 05/05/2020.
//

#ifndef SFML_TEST_RANDOMGENERATOR_H
#define SFML_TEST_RANDOMGENERATOR_H

int getRandomInt(const int & min, const int & max);


#endif //SFML_TEST_RANDOMGENERATOR_H
